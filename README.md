<!--- OASIS_START --->
<!--- DO NOT EDIT (digest: f1b0331dfcbba0e361627569042612c3) --->

OcLaunch - Launch commands automagically
========================================

[![licence
CeCILL](https://img.shields.io/badge/licence-CeCILL-blue.svg)](http://oclaunch.eu.org/floss-under-cecill)
[![command
line](https://img.shields.io/badge/command-line-lightgrey.svg)](http://oclaunch.eu.org/videos)
[![platform UNIX (esp.
LINUX)](https://img.shields.io/badge/platform-UNIX_\(esp._LINUX\)-lightgrey.svg)](http://download.tuxfamily.org/oclaunch/oclaunch.xml)
[![language
OCaml](https://img.shields.io/badge/language-OCaml-orange.svg)](http://www.ocaml.org/)
[![opam
oclaunch](https://img.shields.io/badge/opam-oclaunch-red.svg)](http://opam.ocaml.org/packages/oclaunch/oclaunch.0.2.2/)
[![Getting
help](https://img.shields.io/badge/Get-Help!-orange.svg)](http://www.oclaunch.eu.org/help.html)
<hr/><p>OcLaunch is a command-line tool to launch successively (each time the
program is called) commands. It is designed to be used with any program,
interactive or not. Feedback is welcome at *contact@oclaunch.eu.org*.
Detailed presentation at http://ocla.ml.<br/> Try it, it works
automagically!</p><p>For example, here is a typical session (you open a
terminal emulator between each item): <ul> <li>You open your first terminal,
your chat client is opened,</li> <li>On second launch of a terminal, your
task list is displayed,</li> <li>On third launch, everything has been done.
You will not see anything more.</li> </ul></p>

See the file [INSTALL.md](INSTALL.md) for building and installation
instructions.

[Home page](https://oclaunch.eu.org)

Copyright and license
---------------------

(C) 2014-2016 Joly Clément

OcLaunch is distributed under the terms of the CEA-CNRS-INRIA Logiciel Libre.

See [LICENSE](LICENSE) for more information.

<!--- OASIS_STOP --->
