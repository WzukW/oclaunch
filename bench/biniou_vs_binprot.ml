(******************************************************************************)
(* Copyright © Joly Clément, 2016                                        *)
(*                                                                            *)
(*  leowzukw@oclaunch.eu.org                                                  *)
(*                                                                            *)
(*  Ce logiciel est un programme informatique servant à exécuter              *)
(*  automatiquement des programmes à l'ouverture du terminal.                 *)
(*                                                                            *)
(*  Ce logiciel est régi par la licence CeCILL soumise au droit français et   *)
(*  respectant les principes de diffusion des logiciels libres. Vous pouvez   *)
(*  utiliser, modifier et/ou redistribuer ce programme sous les conditions    *)
(*  de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA    *)
(*  sur le site "http://www.cecill.info".                                     *)
(*                                                                            *)
(*  En contrepartie de l'accessibilité au code source et des droits de copie, *)
(*  de modification et de redistribution accordés par cette licence, il n'est *)
(*  offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons, *)
(*  seule une responsabilité restreinte pèse sur l'auteur du programme,  le   *)
(*  titulaire des droits patrimoniaux et les concédants successifs.           *)
(*                                                                            *)
(*  A cet égard  l'attention de l'utilisateur est attirée sur les risques     *)
(*  associés au chargement,  à l'utilisation,  à la modification et/ou au     *)
(*  développement et à la reproduction du logiciel par l'utilisateur étant    *)
(*  donné sa spécificité de logiciel libre, qui peut le rendre complexe à     *)
(*  manipuler et qui le réserve donc à des développeurs et des professionnels *)
(*  avertis possédant  des  connaissances  informatiques approfondies.  Les   *)
(*  utilisateurs sont donc invités à charger  et  tester  l'adéquation  du    *)
(*  logiciel à leurs besoins dans des conditions permettant d'assurer la      *)
(*  sécurité de leurs systèmes et ou de leurs données et, plus généralement,  *)
(*  à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.        *)
(*                                                                            *)
(*  Le fait que vous puissiez accéder à cet en-tête signifie que vous avez    *)
(*  pris connaissance de la licence CeCILL, et que vous en avez accepté les   *)
(*  termes.                                                                   *)
(******************************************************************************)

open Core.Std;;

(* Test which is the faster, to read and write, biniou or core's bin_prot.
 * That's not the only criteria, but we need to know *)
open Core_bench.Std;;

(* Common parts *)
type rc_entry = {
    commands: (string * int);
}
[@@deriving bin_io];;
type rc_name = string
[@@deriving bin_io];;
type tmp_file = {
  rc: (rc_name * (rc_entry list)) list;
  daemon: int;
}
[@@deriving bin_io];;

(* XXX Copy to give the same data, marked with another type *)
let tmp_data_bin : tmp_file = {
  rc = [
    ("./dev1.scm",
     [
        { commands = ("bump -w daemon,rc,commands /tmp/v033", 1) };
        { commands = ("bdmp -w daemon,rc,commands /tmp/v033", 1) };
        { commands = ("bdup -w daemon,rc,commands /tmp/v033", 1) };
        { commands = ("bdump -aemon,rc,commands /tmp/v033", 1) };
        { commands = ("bdump -,commands /tmp/v033", 1) };
        { commands = ("bdump -,commands /tmp/v033", 1) };
        { commands = ("bdump -p/v033", 1) };
        { commands = ("bdump -,commands /tmp/v033", 1) };
        { commands = ("bdummmands /tmp/v033", 1) };
        { commands = ("echo \"Finish\"", 1) };
        { commands = ("free -h", 1) };
        { commands = ("du -sh ./_build/src/oclaunch.native", 1) };
        { commands = ("task", 1) };
        { commands = ("ydump dev.json", 1) }
     ]);
    ("./dev2.scm",
     [
        { commands = ("Voluptatem occaecati cumque voluptatem voluptatem itaque dolorum. Incidunt voluptas et a qui repellat est omnis. Cupiditate nesciunt perspiciatis dolores laboriosam asperiores ad corporis. Deserunt fugiat nisi est.", 1) };
        { commands = ("Maxime assumenda quo tempora. Ad necessitatibus quis et possimus saepe. Adipisci doloremque omnis repudiandae. Ad enim qui est nemo. Qui dolorem aut quibusdam fugiat est dolores excepturi aut.", 1) };
        { commands = ("Earum error est et repudiandae impedit illo explicabo sint. Magni accusamus dolorem animi sed unde soluta ex rerum. Quos voluptas labore quis saepe. Dolorem esse sunt at rerum. Sit non aut dolores sint est nam. Voluptatem autem eos ut voluptate sint dolores.", 1) };
        { commands = ("Et aut dolorem quam quo minus velit omnis facilis. Rerum quos consectetur velit nihil distinctio in eligendi. Ut optio deserunt et praesentium. Quibusdam veniam laudantium error consequatur.", 1) };
        { commands = ("Occaecati optio est ut. Ratione et perspiciatis deserunt nihil vitae dignissimos. Tempore animi dolorem aut totam non laboriosam quis in. Pariatur quam pariatur eum. Odit officiis ipsa omnis fugit voluptatem corrupti deleniti. Nemo asperiores commodi quae explicabo temporibus ipsam autem.", 1) };
        { commands = ("bdump -w daemon,rc,commands /tmp/v033", 1) };
        { commands = ("echo \"Finish\"", 1) };
        { commands = ("free -h", 1) };
        { commands = ("du -sh ./_build/src/oclaunch.native", 1) };
        { commands = ("task", 1) };
        { commands = ("ydump dev.json", 1) }
     ]);
    ("lipsum",
     ([
        { commands = ("Donec in", 1) };
        { commands = ("nisl", 1) };
        { commands = ("mattis,", 1) };
        { commands = ("scelerisque", 1) };
        { commands = ("ipsum a,", 1) };
        { commands = ("porttitor", 1) };
        { commands = ("diam.", 1) };
        { commands = ("Vestibulum", 1) };
        { commands = ("sed purus", 1) };
        { commands = ("at arcu", 1) };
        { commands = ("iaculis", 1) };
        { commands = ("condimentum.", 1) };
        { commands = ("Praesent", 1) };
        { commands = ("dictum", 1) };
        { commands = ("lacus non", 1) };
        { commands = ("justo", 1) };
        { commands = ("feugiat", 1) };
        { commands = ("sollicitudin.", 1) };
        { commands = ("Fusce", 1) };
        { commands = ("eleifend", 1) };
        { commands = ("malesuada", 1) };
        { commands = ("venenatis.", 1) };
        { commands = ("Integer", 1) };
        { commands = ("fermentum", 1) };
        { commands = ("feugiat", 1) };
        { commands = ("dui, eu", 1) };
        { commands = ("tincidunt", 1) };
        { commands = ("dui", 1) };
        { commands = ("pharetra", 1) };
        { commands = ("ac. Aenean", 1) };
        { commands = ("egestas", 1) };
        { commands = ("nibh eu", 1) };
        { commands = ("dui", 1) };
        { commands = ("ultricies", 1) };
        { commands = ("gravida.", 1) };
        { commands = ("Lorem", 1) };
        { commands = ("ipsum", 1) };
        { commands = ("dolor sit", 1) };
        { commands = ("amet,", 1) };
        { commands = ("consectetur", 1) };
        { commands = ("adipiscing", 1) };
        { commands = ("elit. Cras", 1) };
        { commands = ("quis diam", 1) };
        { commands = ("accumsan,", 1) };
        { commands = ("cursus", 1) };
        { commands = ("purus", 1) };
        { commands = ("quis,", 1) };
        { commands = ("efficitur", 1) };
        { commands = ("mi.", 1) };
        { commands = ("Vestibulum", 1) };
        { commands = ("eleifend", 1) };
        { commands = ("nisi", 1) };
        { commands = ("risus, ut", 1) };
        { commands = ("condimentum", 1) };
        { commands = ("orci", 1) };
        { commands = ("malesuada", 1) };
        { commands = ("a. Nunc", 1) };
        { commands = ("risus", 1) };
        { commands = ("urna,", 1) };
        { commands = ("tempor id", 1) };
        { commands = ("dui in,", 1) };
        { commands = ("blandit", 1) };
        { commands = ("ullamcorper", 1) };
        { commands = ("augue.", 1) };
        { commands = ("Phasellus", 1) };
        { commands = ("ut ex", 1) };
        { commands = ("ullamcorper,", 1) };
        { commands = ("sollicitudin", 1) };
        { commands = ("justo", 1) };
        { commands = ("luctus,", 1) };
        { commands = ("pharetra", 1) };
        { commands = ("felis.", 1) };
        { commands = ("Phasellus", 1) };
        { commands = ("convallis", 1) };
        { commands = ("velit mi.", 1) };
        { commands = ("Vestibulum", 1) };
        { commands = ("vel dui", 1) };
        { commands = ("mauris.", 1) };
        { commands = ("Donec", 1) };
        { commands = ("vestibulum", 1) };
        { commands = ("tempus", 1) };
        { commands = ("justo vel", 1) };
        { commands = ("pharetra.", 1) };
        { commands = ("Cum sociis", 1) };
        { commands = ("natoque", 1) };
        { commands = ("penatibus", 1) };
        { commands = ("et magnis", 1) };
        { commands = ("dis", 1) };
        { commands = ("parturient", 1) };
        { commands = ("montes,", 1) };
        { commands = ("nascetur", 1) };
        { commands = ("ridiculus", 1) };
        { commands = ("mus.", 1) };
        { commands = ("Nullam non", 1) };
        { commands = ("dui quis", 1) };
        { commands = ("tellus", 1) };
        { commands = ("pulvinar", 1) };
        { commands = ("convallis", 1) };
        { commands = ("vel eget", 1) };
        { commands = ("mi. Proin", 1) };
        { commands = ("aliquet,", 1) };
        { commands = ("lorem at", 1) };
        { commands = ("auctor", 1) };
        { commands = ("volutpat,", 1) };
        { commands = ("orci est", 1) };
        { commands = ("vehicula", 1) };
        { commands = ("diam, in", 1) };
        { commands = ("sollicitudin", 1) };
        { commands = ("velit", 1) };
        { commands = ("massa et", 1) };
        { commands = ("diam.", 1) };
        { commands = ("Praesent", 1) };
        { commands = ("sed diam", 1) };
        { commands = ("iaculis,", 1) };
        { commands = ("mollis", 1) };
        { commands = ("justo sit", 1) };
        { commands = ("amet,", 1) };
        { commands = ("cursus", 1) };
        { commands = ("augue.", 1) };
        { commands = ("Quisque", 1) };
        { commands = ("ultrices", 1) };
        { commands = ("odio ut", 1) };
        { commands = ("leo", 1) };
        { commands = ("eleifend", 1) };
        { commands = ("faucibus.", 1) };
        { commands = ("Ut", 1) };
        { commands = ("ullamcorper", 1) };
        { commands = ("non magna", 1) };
        { commands = ("a commodo.", 1) };
        { commands = ("Nullam in", 1) };
        { commands = ("orci arcu.", 1) };
        { commands = ("Nunc", 1) };
        { commands = ("iaculis", 1) };
        { commands = ("auctor", 1) };
        { commands = ("lobortis.", 1) };
        { commands = ("Maecenas", 1) };
        { commands = ("laoreet", 1) };
        { commands = ("imperdiet", 1) };
        { commands = ("congue.", 1) };
        { commands = ("Nullam", 1) };
        { commands = ("pellentesque", 1) };
        { commands = ("varius", 1) };
        { commands = ("nunc, sed", 1) };
        { commands = ("tincidunt", 1) };
        { commands = ("nulla", 1) };
        { commands = ("luctus et.", 1) };
        { commands = ("Integer a", 1) };
        { commands = ("risus", 1) };
        { commands = ("urna. Nunc", 1) };
        { commands = ("in auctor", 1) };
        { commands = ("sapien.", 1) };
        { commands = ("Nulla", 1) };
        { commands = ("pellentesque,", 1) };
        { commands = ("orci sit", 1) };
        { commands = ("amet", 1) };
        { commands = ("efficitur", 1) };
        { commands = ("egestas,", 1) };
        { commands = ("quam urna", 1) };
        { commands = ("tempor", 1) };
        { commands = ("nibh, eu", 1) };
        { commands = ("varius", 1) };
        { commands = ("dolor erat", 1) };
        { commands = ("nec ex.", 1) };
     ]))
  ];
  daemon = 2
};;

let tmp_data : Tmp_biniou_t.tmp_file = {
  rc = [
    ("./dev1.scm",
     [
        { commands = ("bump -w daemon,rc,commands /tmp/v033", 1) };
        { commands = ("bdmp -w daemon,rc,commands /tmp/v033", 1) };
        { commands = ("bdup -w daemon,rc,commands /tmp/v033", 1) };
        { commands = ("bdump -aemon,rc,commands /tmp/v033", 1) };
        { commands = ("bdump -,commands /tmp/v033", 1) };
        { commands = ("bdump -,commands /tmp/v033", 1) };
        { commands = ("bdump -p/v033", 1) };
        { commands = ("bdump -,commands /tmp/v033", 1) };
        { commands = ("bdummmands /tmp/v033", 1) };
        { commands = ("echo \"Finish\"", 1) };
        { commands = ("free -h", 1) };
        { commands = ("du -sh ./_build/src/oclaunch.native", 1) };
        { commands = ("task", 1) };
        { commands = ("ydump dev.json", 1) }
     ]);
    ("./dev2.scm",
     [
        { commands = ("Voluptatem occaecati cumque voluptatem voluptatem itaque dolorum. Incidunt voluptas et a qui repellat est omnis. Cupiditate nesciunt perspiciatis dolores laboriosam asperiores ad corporis. Deserunt fugiat nisi est.", 1) };
        { commands = ("Maxime assumenda quo tempora. Ad necessitatibus quis et possimus saepe. Adipisci doloremque omnis repudiandae. Ad enim qui est nemo. Qui dolorem aut quibusdam fugiat est dolores excepturi aut.", 1) };
        { commands = ("Earum error est et repudiandae impedit illo explicabo sint. Magni accusamus dolorem animi sed unde soluta ex rerum. Quos voluptas labore quis saepe. Dolorem esse sunt at rerum. Sit non aut dolores sint est nam. Voluptatem autem eos ut voluptate sint dolores.", 1) };
        { commands = ("Et aut dolorem quam quo minus velit omnis facilis. Rerum quos consectetur velit nihil distinctio in eligendi. Ut optio deserunt et praesentium. Quibusdam veniam laudantium error consequatur.", 1) };
        { commands = ("Occaecati optio est ut. Ratione et perspiciatis deserunt nihil vitae dignissimos. Tempore animi dolorem aut totam non laboriosam quis in. Pariatur quam pariatur eum. Odit officiis ipsa omnis fugit voluptatem corrupti deleniti. Nemo asperiores commodi quae explicabo temporibus ipsam autem.", 1) };
        { commands = ("bdump -w daemon,rc,commands /tmp/v033", 1) };
        { commands = ("echo \"Finish\"", 1) };
        { commands = ("free -h", 1) };
        { commands = ("du -sh ./_build/src/oclaunch.native", 1) };
        { commands = ("task", 1) };
        { commands = ("ydump dev.json", 1) }
     ]);
    ("lipsum",
     ([
        { commands = ("Donec in", 1) };
        { commands = ("nisl", 1) };
        { commands = ("mattis,", 1) };
        { commands = ("scelerisque", 1) };
        { commands = ("ipsum a,", 1) };
        { commands = ("porttitor", 1) };
        { commands = ("diam.", 1) };
        { commands = ("Vestibulum", 1) };
        { commands = ("sed purus", 1) };
        { commands = ("at arcu", 1) };
        { commands = ("iaculis", 1) };
        { commands = ("condimentum.", 1) };
        { commands = ("Praesent", 1) };
        { commands = ("dictum", 1) };
        { commands = ("lacus non", 1) };
        { commands = ("justo", 1) };
        { commands = ("feugiat", 1) };
        { commands = ("sollicitudin.", 1) };
        { commands = ("Fusce", 1) };
        { commands = ("eleifend", 1) };
        { commands = ("malesuada", 1) };
        { commands = ("venenatis.", 1) };
        { commands = ("Integer", 1) };
        { commands = ("fermentum", 1) };
        { commands = ("feugiat", 1) };
        { commands = ("dui, eu", 1) };
        { commands = ("tincidunt", 1) };
        { commands = ("dui", 1) };
        { commands = ("pharetra", 1) };
        { commands = ("ac. Aenean", 1) };
        { commands = ("egestas", 1) };
        { commands = ("nibh eu", 1) };
        { commands = ("dui", 1) };
        { commands = ("ultricies", 1) };
        { commands = ("gravida.", 1) };
        { commands = ("Lorem", 1) };
        { commands = ("ipsum", 1) };
        { commands = ("dolor sit", 1) };
        { commands = ("amet,", 1) };
        { commands = ("consectetur", 1) };
        { commands = ("adipiscing", 1) };
        { commands = ("elit. Cras", 1) };
        { commands = ("quis diam", 1) };
        { commands = ("accumsan,", 1) };
        { commands = ("cursus", 1) };
        { commands = ("purus", 1) };
        { commands = ("quis,", 1) };
        { commands = ("efficitur", 1) };
        { commands = ("mi.", 1) };
        { commands = ("Vestibulum", 1) };
        { commands = ("eleifend", 1) };
        { commands = ("nisi", 1) };
        { commands = ("risus, ut", 1) };
        { commands = ("condimentum", 1) };
        { commands = ("orci", 1) };
        { commands = ("malesuada", 1) };
        { commands = ("a. Nunc", 1) };
        { commands = ("risus", 1) };
        { commands = ("urna,", 1) };
        { commands = ("tempor id", 1) };
        { commands = ("dui in,", 1) };
        { commands = ("blandit", 1) };
        { commands = ("ullamcorper", 1) };
        { commands = ("augue.", 1) };
        { commands = ("Phasellus", 1) };
        { commands = ("ut ex", 1) };
        { commands = ("ullamcorper,", 1) };
        { commands = ("sollicitudin", 1) };
        { commands = ("justo", 1) };
        { commands = ("luctus,", 1) };
        { commands = ("pharetra", 1) };
        { commands = ("felis.", 1) };
        { commands = ("Phasellus", 1) };
        { commands = ("convallis", 1) };
        { commands = ("velit mi.", 1) };
        { commands = ("Vestibulum", 1) };
        { commands = ("vel dui", 1) };
        { commands = ("mauris.", 1) };
        { commands = ("Donec", 1) };
        { commands = ("vestibulum", 1) };
        { commands = ("tempus", 1) };
        { commands = ("justo vel", 1) };
        { commands = ("pharetra.", 1) };
        { commands = ("Cum sociis", 1) };
        { commands = ("natoque", 1) };
        { commands = ("penatibus", 1) };
        { commands = ("et magnis", 1) };
        { commands = ("dis", 1) };
        { commands = ("parturient", 1) };
        { commands = ("montes,", 1) };
        { commands = ("nascetur", 1) };
        { commands = ("ridiculus", 1) };
        { commands = ("mus.", 1) };
        { commands = ("Nullam non", 1) };
        { commands = ("dui quis", 1) };
        { commands = ("tellus", 1) };
        { commands = ("pulvinar", 1) };
        { commands = ("convallis", 1) };
        { commands = ("vel eget", 1) };
        { commands = ("mi. Proin", 1) };
        { commands = ("aliquet,", 1) };
        { commands = ("lorem at", 1) };
        { commands = ("auctor", 1) };
        { commands = ("volutpat,", 1) };
        { commands = ("orci est", 1) };
        { commands = ("vehicula", 1) };
        { commands = ("diam, in", 1) };
        { commands = ("sollicitudin", 1) };
        { commands = ("velit", 1) };
        { commands = ("massa et", 1) };
        { commands = ("diam.", 1) };
        { commands = ("Praesent", 1) };
        { commands = ("sed diam", 1) };
        { commands = ("iaculis,", 1) };
        { commands = ("mollis", 1) };
        { commands = ("justo sit", 1) };
        { commands = ("amet,", 1) };
        { commands = ("cursus", 1) };
        { commands = ("augue.", 1) };
        { commands = ("Quisque", 1) };
        { commands = ("ultrices", 1) };
        { commands = ("odio ut", 1) };
        { commands = ("leo", 1) };
        { commands = ("eleifend", 1) };
        { commands = ("faucibus.", 1) };
        { commands = ("Ut", 1) };
        { commands = ("ullamcorper", 1) };
        { commands = ("non magna", 1) };
        { commands = ("a commodo.", 1) };
        { commands = ("Nullam in", 1) };
        { commands = ("orci arcu.", 1) };
        { commands = ("Nunc", 1) };
        { commands = ("iaculis", 1) };
        { commands = ("auctor", 1) };
        { commands = ("lobortis.", 1) };
        { commands = ("Maecenas", 1) };
        { commands = ("laoreet", 1) };
        { commands = ("imperdiet", 1) };
        { commands = ("congue.", 1) };
        { commands = ("Nullam", 1) };
        { commands = ("pellentesque", 1) };
        { commands = ("varius", 1) };
        { commands = ("nunc, sed", 1) };
        { commands = ("tincidunt", 1) };
        { commands = ("nulla", 1) };
        { commands = ("luctus et.", 1) };
        { commands = ("Integer a", 1) };
        { commands = ("risus", 1) };
        { commands = ("urna. Nunc", 1) };
        { commands = ("in auctor", 1) };
        { commands = ("sapien.", 1) };
        { commands = ("Nulla", 1) };
        { commands = ("pellentesque,", 1) };
        { commands = ("orci sit", 1) };
        { commands = ("amet", 1) };
        { commands = ("efficitur", 1) };
        { commands = ("egestas,", 1) };
        { commands = ("quam urna", 1) };
        { commands = ("tempor", 1) };
        { commands = ("nibh, eu", 1) };
        { commands = ("varius", 1) };
        { commands = ("dolor erat", 1) };
        { commands = ("nec ex.", 1) };
     ]))
  ];
  daemon = 2
};;

let tmp_base_name = "/tmp/bvsbp"

(* With biniou *)
let serialise_biniou data =
  Tmp_biniou_b.string_of_tmp_file data
;;
let deserialise_biniou data =
  Tmp_biniou_b.tmp_file_of_string data
;;
let biniou () =
  serialise_biniou tmp_data
  |> deserialise_biniou
  (* Compare with source data *)
  |> fun read -> assert (read = tmp_data)
;;
let write_biniou () =
  let name = tmp_base_name ^ ".bi" in
  serialise_biniou tmp_data
  |> (fun data -> Out_channel.write_all ~data name)
;;

(* With bin_prot *)
let serialise_binprot data =
  let module Tmp_bi = struct
    type t = tmp_file [@@deriving bin_io]
  end in
  Binable.to_string (module Tmp_bi) data
;;
let deserialise_binprot data =
  let module Tmp_bi = struct
    type t = tmp_file [@@deriving bin_io]
  end in
  Binable.of_string (module Tmp_bi) data
;;
let bin_prot () =
  serialise_binprot tmp_data_bin
  |> deserialise_binprot
  (* Compare with source data *)
  |> fun read -> assert (read = tmp_data_bin)
;;
let write_binprot () =
  let name = tmp_base_name ^ ".bp" in
  serialise_binprot tmp_data_bin
  |> (fun data -> Out_channel.write_all ~data name)
;;

(* Serialise and deserialise data (with checks) *)
let tests = [
  "Binou", biniou;
  "Bin_prot", bin_prot;
]

let () =
  (* Write files once *)
  write_biniou ();
  write_binprot ();
  (* Run tests *)
  List.map tests ~f:(fun (name,test) -> Bench.Test.create ~name test)
  |> Bench.make_command
  |> Command.run
