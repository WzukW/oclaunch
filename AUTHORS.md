<!--- OASIS_START --->
<!--- DO NOT EDIT (digest: 7d7e6e9b59529ebaf8057deb13b160d9) --->

Authors of OcLaunch:

* Joly Clément <leowzukw@oclaunch.eu.org>

Current maintainers of OcLaunch:

* Joly Clément <leowzukw@oclaunch.eu.org>

<!--- OASIS_STOP --->
