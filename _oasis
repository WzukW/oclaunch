OASISFormat: 0.4
Name:        OcLaunch
Version:     0.3.0-rc1
Synopsis:    Launch commands automagically
Authors:     Joly Clément <leowzukw@oclaunch.eu.org>
Maintainers: Joly Clément <leowzukw@oclaunch.eu.org>
License:     CeCILL
LicenseFile: LICENSE
Copyrights: (C) 2014-2016 Joly Clément
Homepage: https://oclaunch.eu.org
BuildTools: ocamlbuild
Plugins: StdFiles (0.4), DevFiles (0.4)
XStdFilesREADME: true
XStdFilesINSTALL: true
XStdFilesAUTHORS: true
XOCamlbuildExtraArgs: -use-ocamlfind -tag \"ppx(ppx-jane -as-ppx)\" -tag thread -tag debug -tag bin_annot -tag short_paths -cflags \"-w A-4-33-40-41-42-43-34-44\" -cflags -strict-sequence
AlphaFeatures: stdfiles_markdown, compiled_setup_ml, ocamlbuild_more_args
Description: [![licence CeCILL](https://img.shields.io/badge/licence-CeCILL-blue.svg)](http://oclaunch.eu.org/floss-under-cecill) [![command line](https://img.shields.io/badge/command-line-lightgrey.svg)](http://oclaunch.eu.org/videos) [![platform UNIX (esp. LINUX)](https://img.shields.io/badge/platform-UNIX_\(esp._LINUX\)-lightgrey.svg)](http://download.tuxfamily.org/oclaunch/oclaunch.xml) [![language OCaml](https://img.shields.io/badge/language-OCaml-orange.svg)](http://www.ocaml.org/) [![opam oclaunch](https://img.shields.io/badge/opam-oclaunch-red.svg)](http://opam.ocaml.org/packages/oclaunch/oclaunch.0.2.2/) [![Getting help](https://img.shields.io/badge/Get-Help!-orange.svg)](http://www.oclaunch.eu.org/help.html) <hr/><p>OcLaunch is a command-line tool to launch successively (each time the program is called) commands. It is designed to be used with any program, interactive or not. Feedback is welcome at *contact@oclaunch.eu.org*. Detailed presentation at http://ocla.ml.<br/> Try it, it works automagically!</p><p>For example, here is a typical session (you open a terminal emulator between each item): <ul> <li>You open your first terminal, your chat client is opened,</li> <li>On second launch of a terminal, your task list is displayed,</li> <li>On third launch, everything has been done. You will not see anything more.</li> </ul></p>

Executable oclaunch
  Path:       src
  MainIs:     oclaunch.ml
  BuildDepends: core, textutils, threads, re2, atdgen, yojson
  CompiledObject: best

Executable run_test
  Path: src
  MainIs: test/test.ml
  CompiledObject: best
  Build$: flag(tests)
  Install: false
  BuildDepends: alcotest, oUnit, core, textutils, atdgen, threads, re2

Test alcotests
  Run$: flag(tests)
  Command: $run_test -q
  WorkingDirectory: src/test

