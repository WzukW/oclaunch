#!/bin/sh

# OCaml version is assuming to be set (for instance as system compiler) if
# nothing is passed

# Inspired by https://github.com/ocaml/ocaml-ci-scripts

# Use -y with evry opam command
export OPAMYES=true
# Installing opam
comp=${1:-system}
opam init --comp="${comp}"
eval `opam config env`

# Versions
echo "= Versions ="
echo "opam --version"
opam --version
echo "opam --git-version"
opam --git-version
echo "ocaml -version"
ocaml -version
echo "============"

echo "= Dependancies ="
# ocamlfind is mandatory to build
opam install ocamlfind
# XXX Manually install development dependancies, not yet supported (wait opam 1.3)
opam install alcotest oUnit

# Installing dependancies and testing installation
opam pin add oclaunch .
echo "============"

# Run test if OC_NOTEST is false
if [ ! ${OC_NOTEST} ]; then
  # Build OcLaunch only
  ./configure
  make
else
  # Building OcLaunch and running tests
  ./configure --enable-tests
  make test

  # Test the produced binary
  oclaunch -version
  echo "En" | oclaunch
fi
