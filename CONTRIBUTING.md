# Contributions are most welcome, not only for the code !

See [the website](https://oclaunch.eu.org/contribute) for details.

By participating in this project you agree to abide by its
[rules](https://www.oclaunch.eu.org/rules).
