(******************************************************************************)
(* Copyright © Joly Clément, 2014-2016                                        *)
(*                                                                            *)
(*  leowzukw@oclaunch.eu.org                                                  *)
(*                                                                            *)
(*  Ce logiciel est un programme informatique servant à exécuter              *)
(*  automatiquement des programmes à l'ouverture du terminal.                 *)
(*                                                                            *)
(*  Ce logiciel est régi par la licence CeCILL soumise au droit français et   *)
(*  respectant les principes de diffusion des logiciels libres. Vous pouvez   *)
(*  utiliser, modifier et/ou redistribuer ce programme sous les conditions    *)
(*  de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA    *)
(*  sur le site "http://www.cecill.info".                                     *)
(*                                                                            *)
(*  En contrepartie de l'accessibilité au code source et des droits de copie, *)
(*  de modification et de redistribution accordés par cette licence, il n'est *)
(*  offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons, *)
(*  seule une responsabilité restreinte pèse sur l'auteur du programme,  le   *)
(*  titulaire des droits patrimoniaux et les concédants successifs.           *)
(*                                                                            *)
(*  A cet égard  l'attention de l'utilisateur est attirée sur les risques     *)
(*  associés au chargement,  à l'utilisation,  à la modification et/ou au     *)
(*  développement et à la reproduction du logiciel par l'utilisateur étant    *)
(*  donné sa spécificité de logiciel libre, qui peut le rendre complexe à     *)
(*  manipuler et qui le réserve donc à des développeurs et des professionnels *)
(*  avertis possédant  des  connaissances  informatiques approfondies.  Les   *)
(*  utilisateurs sont donc invités à charger  et  tester  l'adéquation  du    *)
(*  logiciel à leurs besoins dans des conditions permettant d'assurer la      *)
(*  sécurité de leurs systèmes et ou de leurs données et, plus généralement,  *)
(*  à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.        *)
(*                                                                            *)
(*  Le fait que vous puissiez accéder à cet en-tête signifie que vous avez    *)
(*  pris connaissance de la licence CeCILL, et que vous en avez accepté les   *)
(*  termes.                                                                   *)
(******************************************************************************)

open Core.Std;;

(* The module containing the step run when the program is
 * used without argument or with run command *)

(* When there is nothing in rc file *)
let nothing_in_rc () =
  Messages.ok "There is nothing in your RC file!";
  Messages.tips "You can add entries with 'edit' or 'add' subcommand.";
;;

(* If no command was found, all has been launched *)
let no_cmd_found () =
  Messages.ok "All has been launched!";
  Messages.tips "You can reset with 'reset-all' subcommand";
;;

(* Run given (num) item *)
let run_item ~rc num =
  rc#entry ~n:num
  |> function
  | None -> Messages.warning "Your number is out of bound"
  | Some entry ->
    let cmd_to_exec = entry#command in
    Exec_cmd.execute cmd_to_exec;
;;

(* Execute each item (one after the other) in config file *)
let exec_each ~tmp =
  if not (Tmp_file.is_disabled ~tmp)
  then
    Exec_cmd.what_next ~tmp
    |> function
    | Exec_cmd.Empty -> nothing_in_rc ()
    | Finish -> no_cmd_found ()
    | A cmd_to_exec -> Exec_cmd.execute cmd_to_exec
  else Messages.debug "Disabled"
;;

(* cmd_number is the number of the command the user wants
 * to execute *)
let run ~(rc:Rc.t) cmd_number =

  (* Wait for another oclaunch instance which could launch the same program at
   * the same time and then lock *)
  Messages.debug "Waiting for locker";
  Lock.wait ();
  Messages.debug "Locked";

  let tmp = Tmp_file.init () in
  match cmd_number with
  | None -> exec_each ~tmp; Lock.remove ()
  | Some num -> run_item ~rc num
;;
