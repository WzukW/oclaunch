(******************************************************************************)
(* Copyright © Joly Clément, 2016                                             *)
(*                                                                            *)
(*  leowzukw@oclaunch.eu.org                                                  *)
(*                                                                            *)
(*  Ce logiciel est un programme informatique servant à exécuter              *)
(*  automatiquement des programmes à l'ouverture du terminal.                 *)
(*                                                                            *)
(*  Ce logiciel est régi par la licence CeCILL soumise au droit français et   *)
(*  respectant les principes de diffusion des logiciels libres. Vous pouvez   *)
(*  utiliser, modifier et/ou redistribuer ce programme sous les conditions    *)
(*  de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA    *)
(*  sur le site "http://www.cecill.info".                                     *)
(*                                                                            *)
(*  En contrepartie de l'accessibilité au code source et des droits de copie, *)
(*  de modification et de redistribution accordés par cette licence, il n'est *)
(*  offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons, *)
(*  seule une responsabilité restreinte pèse sur l'auteur du programme,  le   *)
(*  titulaire des droits patrimoniaux et les concédants successifs.           *)
(*                                                                            *)
(*  A cet égard  l'attention de l'utilisateur est attirée sur les risques     *)
(*  associés au chargement,  à l'utilisation,  à la modification et/ou au     *)
(*  développement et à la reproduction du logiciel par l'utilisateur étant    *)
(*  donné sa spécificité de logiciel libre, qui peut le rendre complexe à     *)
(*  manipuler et qui le réserve donc à des développeurs et des professionnels *)
(*  avertis possédant  des  connaissances  informatiques approfondies.  Les   *)
(*  utilisateurs sont donc invités à charger  et  tester  l'adéquation  du    *)
(*  logiciel à leurs besoins dans des conditions permettant d'assurer la      *)
(*  sécurité de leurs systèmes et ou de leurs données et, plus généralement,  *)
(*  à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.        *)
(*                                                                            *)
(*  Le fait que vous puissiez accéder à cet en-tête signifie que vous avez    *)
(*  pris connaissance de la licence CeCILL, et que vous en avez accepté les   *)
(*  termes.                                                                   *)
(******************************************************************************)

open Core.Std;;

module Test :
  sig
    type basic_rc_t
    val basic_rc_of_sexp : Sexp.t -> basic_rc_t
    val basic_template : string
  end

val welcome_msg : string

val equal : < equal : 'a -> bool; .. > -> 'a -> bool

(* TODO Improve documentation *)
class tag : string -> string list ->
  object ('self)
    val arguments : string list
    val name : string
    method arguments : string list
    method name : string
    method equal : 'self -> bool
end
class entry : ?tags:tag list -> string ->
object ('self)
  val command : string
  val tags : tag list
  method command : string
  method tags : tag list
  method change_command : string -> 'self
  method change_tags : tag list -> 'self
  method equal : 'self -> bool
end
type setting = < key : string; value : string >;;
type t = <
  entries : entry list;
  common_tags : tag list;
  settings : setting list;
  change_entries : entry list -> t;
  add_common_tags : tag list -> t;
  add_entries : entry list -> t;
  add_settings : setting list -> t;
  change_entry : int -> f:(entry -> entry) -> t;
  change_name : string -> t;
  entry : n:int -> entry option;
  filter_map_common_tags : f:(tag -> tag option) -> t;
  filter_map_entries : f:(int -> entry -> entry option) -> t;
  filter_map_settings : f:(setting -> setting option) -> t;
  map_common_tags : f:(tag -> tag) -> t;
  map_entries : f:(int -> entry -> entry) -> t;
  map_settings : f:(setting -> setting) -> t;
  get_name : string;
  to_string : string;
  read_common_tags : f:(tag -> unit) -> unit;
  read_settings : f:(setting -> unit) -> unit;
  remove_entry : int -> t;
  write : unit
>;;

val init : ?rc:string lazy_t -> unit -> t

val empty_entry : unit -> entry

val import : from:(string Lazy.t) -> to_file:(string Lazy.t) -> unit
